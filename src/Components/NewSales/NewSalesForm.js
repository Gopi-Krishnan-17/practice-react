import React from "react";
import './NewSalesForm.css';
const NewSalesForm = () =>
{
    <form>
            <div className="new-sales-total">
                <div className="new-sales-ind">
                    <label>Name</label>
                    <input type='text' />
                </div>
                <div className="new-sales-ind">
                    <label>Amount</label>
                    <input type='number' min='50,000' />
                </div>
                <div className="new-sales-ind">
                    <label>Date</label>
                    <input type='date' min='2020-01-01' max='2022-12-31' />
                </div>
                <div className="actions">
                    <button type="submit">Add Sold Bike</button></div>
            </div>
        </form>
}
export default NewSalesForm;