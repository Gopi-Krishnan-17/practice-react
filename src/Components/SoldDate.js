import './SoldDate.css';
const SoldDate = (props) =>
{
    const year = props.date.getFullYear();
    const month= props.date.toLocaleString('en-US',{month:'long'});
    const day=props.date.toLocaleString('en-US',{day:'2-digit'});
return(
    <div className='sold-date'>
    <div className='sold-month'>{month}</div>
    <div className='sold-year'>{year}</div>
    <div className='sold-day'>{day}</div>
</div>
);
}
export default SoldDate;      