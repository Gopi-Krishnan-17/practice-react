import React, {useState} from 'react';
import './SoldItem.css';
import SoldDate from './SoldDate.js';
const SoldItem = (props) =>
{
    const [name, setName] = useState(props.name);

    const clickHandler = () =>
    {
        setName('Name Changed!');
        console.log(name);
    };
    return(
        <div>
            <div className="sold-bike">
            <SoldDate date={props.date}/>
            <div className="sold-bike_name"><h2>{name}</h2></div>
            <div className="sold-bike_price">Rs. {props.amount}</div>
            <button onClick={clickHandler}>Change Name</button>
            </div>
        </div>
    );
}
export default SoldItem;