import './App.css';
import './Components/SoldItem';
import SoldItem from './Components/SoldItem';
import NewSales from './Components/NewSales/NewSales';

const App = () =>
{
  const sold = [
    {
      id: 's1',
      name: 'Yamaha',
      date: new Date(2022, 5,12),
      amount:'2,20,000',
    },
    {
      id: 's2',
      name: 'Honda',
      date: new Date(2022, 6,28),
      amount:'1,40,000',
    },
    {
      id: 's3',
      name: 'Suzuki',
      date: new Date(2022, 3,1),
      amount:'1,10,000',
    },
    {
      id: 's4',
      name: 'TVS',
      date: new Date(2022, 4,19),
      amount:'1,00,000',
    },
    {
      id: 's5',
      name: 'Royal Enfield',
      date: new Date(2022, 8,10),
      amount:'3,50,000',
    }
  ]
  return (
    <div className='form'>
      <NewSales />
      <div>
      <SoldItem 
      name={sold[0].name}
      date={sold[0].date}
      amount={sold[0].amount}
      ></SoldItem>

      <SoldItem 
      name={sold[1].name}
      date={sold[1].date}
      amount={sold[1].amount}
      ></SoldItem>
      <SoldItem 
      name={sold[2].name}
      date={sold[2].date}
      amount={sold[2].amount}
      ></SoldItem>

      <SoldItem 
      name={sold[3].name}
      date={sold[3].date}
      amount={sold[3].amount}
      ></SoldItem>

      <SoldItem 
      name={sold[4].name}
      date={sold[4].date}
      amount={sold[4].amount}
      ></SoldItem>
      </div>
    </div>
  );
}

export default App;
